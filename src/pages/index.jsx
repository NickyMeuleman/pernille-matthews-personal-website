import React from "react";
import Home from "../components/Home/Home";
import {ContentContainer} from "../components/Common/Container";
// import { head } from "../components/Common/Head";
// import { useSiteMetadata } from "../hooks/use-site-metadata";
import "katex/dist/katex.min.css";

// TODO: Add a head component to the layout
const Index = () => {
  // const { site } = useSiteMetadata();
  return (
      <>
          <ContentContainer>
              {/*<div>{ site.title }</div>*/}
              <Home />
          </ContentContainer>
      </>
  );
};

export default Index;