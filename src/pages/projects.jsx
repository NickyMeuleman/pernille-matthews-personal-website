import React from "react"
import {ContentContainer} from "../components/Common/Container";
import { Link } from "gatsby";
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import {Box, CardActionArea, CardMedia, Chip, Stack} from "@mui/material";
import colors from "../shared/colors.js";
import { useProjectsList } from "../hooks/use-projects-list";
import {GatsbyImage} from "gatsby-plugin-image";
import Grid from "@mui/material/Grid";
import "katex/dist/katex.min.css"


export default function Projects() {
    const { nodes } = useProjectsList();
    return (
            <ContentContainer>
                <Grid container sx={{ flexGrow: 1 }} spacing={5}>
                            {nodes.map(({ frontmatter, excerpt }) => (
                                <Grid key={frontmatter} item xs={12} md={6} lg={4}>
                                    <Link to={frontmatter.slug} style={{ textDecoration: 'none' }}>
                                    <Card sx={{  maxWidth: 345,
                                                 bgcolor: colors.purple,
                                                 borderRadius: 3,
                                                 textAlign: "left",
                                                 color: colors.white,
                                                 margin: "0 auto",
                                                 padding: "0.1em",
                                                }}
                                    >
                                        <CardActionArea>
                                            <CardMedia height="250" sx={{ padding: "0 0 0 0", objectFit: "contain" }}>
                                                <GatsbyImage image={frontmatter.featuredImage.childImageSharp.gatsbyImageData} alt={frontmatter.title} />
                                            </CardMedia>

                                           <CardContent>
                                                <Typography gutterBottom  variant="h4" component="div">
                                                    {frontmatter.title}
                                                </Typography>
                                               <Typography variant="subtitle1"component="div" sx={{ color: colors.platinum, }}>
                                                   {frontmatter.date}
                                               </Typography>
                                               <Typography variant="body1" textAlign="left">
                                                   {excerpt}
                                              </Typography>
                                           </CardContent>

                                           <Box sx={{ m: 2 }} >
                                               <Stack direction="row" spacing={1} sx={{ justifyContent: 'space-evenly', }}>
                                                    {frontmatter.tags.map(tag => (
                                                       <Chip label={tag} sx={{ color: colors.white,
                                                           bgcolor: colors.background_black,
                                                           justifyContent: 'space-evenly',
                                                       }} />
                                                   ))}
                                               </Stack>
                                            </Box>
                                        </CardActionArea>
                                    </Card>
                                    </Link>
                                </Grid>
                            ))}
                        </Grid>
            </ContentContainer>
 )
}
