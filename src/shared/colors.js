const colors = {
    background_black: "#282a36",
    grey: "#565761",
    white: "#F8F8F2",
    platinum: "#e2e2dc",
    light_green: "#50fa7b",
    baby_blue: "#8be9fd",
    pastel_blue: "#6272A4",
    blue: "#0189cc",
    pastel_red: "#ff5555",
    pastel_orange: "#FFB86C",
    yellow: "#f1fa8c",
    pink: "#ff79c6",
    purple: "#44475A",
    light_purple: "#bd93f9"


};

export default colors;