// import * as React from "react";
// import { createTheme, ThemeProvider } from "@mui/material";
// import {amber, deepOrange, grey} from "@mui/material/colors";
// import colors from "./colors";
//
// export const ColorModeContext = React.createContext({ toggleColorMode: () => {} });
//
// export const ColorModeContextProvider = ({ children }) => {
//   const [mode, setMode] = React.useState("dark");
//   const toggleColorMode = () => setMode((prevMode) => (prevMode === "dark" ? "light" : "dark"));
//
//   const theme = React.useMemo(
//     () =>
//         createTheme({
//             palette: {
//                 mode,
//                 ...(mode === 'light') ? {
//                     // palette values for light mode
//                     primary: amber,
//                     divider: amber[200],
//                     text: {
//                         primary: grey[900],
//                         secondary: grey[800],
//                     },
//                 } : {
//                     // palette values for dark mode
//                     primary: deepOrange,
//                     divider: deepOrange[700],
//                     background: {
//                         default: colors.background_black,
//                     },
//                     text: {
//                         primary: '#fff',
//                         secondary: grey[500],
//                     },
//                 }
//             },
//             typography: {
//                 fontFamily: 'Consolas',
//             }
//         }), [mode]
//     );
//     return (
//         <ColorModeContext.Provider value={{ toggleColorMode }}>
//             <ThemeProvider theme={theme}>{children}</ThemeProvider>
//         </ColorModeContext.Provider>
//     );
// }
//
// export const useColorMode = () => React.useContext(ColorModeContext);
