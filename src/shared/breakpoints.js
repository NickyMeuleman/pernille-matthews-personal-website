const breakpoints = {
    xs_mobile: `@media (min-width: 0px)`,
    mobile: `@media (min-width: 375px)`,
    tablet: `@media (min-width: 768px)`,
    // xs_mobile: `@media (min-width: 0px)`,
    // mobile: `@media (min-width: 375px)`,
    // tablet: `@media (min-width: 0px)`,
    desktop: `@media (min-width: 1025px)`,
};

export default breakpoints;
