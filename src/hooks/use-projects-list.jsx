import { graphql, useStaticQuery } from "gatsby"

export const useProjectsList = () => {
    const { allMdx } = useStaticQuery( graphql
            `
            query ProjectsQuery {
                allMdx(sort: {order: ASC, fields: frontmatter___date}) {
                    nodes {
                        frontmatter {
                            featuredImage {
                                childImageSharp {
                                    gatsbyImageData(height: 300 quality: 100 layout: CONSTRAINED placeholder: BLURRED)
#                                    fluid(maxWidth: 400, maxHeight: 200, quality: 100) {
#                                        ...GatsbyImageSharpFluid
#                                        ...GatsbyImageSharpFluidLimitPresentationSize
#                                    }
                                }
                            }
                            slug
                            title
                            date
                            tags
                        }
                        excerpt(pruneLength: 200)
                    }
                }
            }
        `
    )
    return allMdx
}

