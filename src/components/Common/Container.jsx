import styled from "styled-components";
import breakpoints from "../../shared/breakpoints";
import { animated } from "react-spring";
import colors from "../../shared/colors";

const Container = styled(animated.div)`
  position: relative;
  grid-column: 2 / 12;
  display: grid;
  grid-row-gap: 8rem;
  margin-bottom: 8rem;

  ${breakpoints.tablet} {
    grid-column: 3 / 11;
  }
`;

export const ContentContainer = styled(Container)`
  margin-top: 5vh;
  
  .mainText {
    font-size: 2rem; 
  }
  
  .imgCol {
    margin-top: 10px
    
  }
  
  .subTitles {
    text-align: center;
    font-size: 2rem;
    margin-top: 20px;
  }
  
  ${breakpoints.desktop} {
    display: grid;
    //column-gap: 4%;
    row-gap: 0;
    grid-template-areas:
      ". left right ."
      ". left LONG ."
  }
  
  grid-auto-rows: min-content;

    .long { 
      grid-area: LONG;
      text-align: center;
      margin-top:20px;
    }
  
    .left {      
      h1 {
        color: ${colors.platinum};
      }
      
      p {
        font-size: 25px;
        line-height: 1.2;
      }
    }
`;


export default Container;
