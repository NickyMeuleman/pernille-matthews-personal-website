import styled from "styled-components";
import colors from "../../shared/colors";
import { transitions } from "../../shared/transitions";
import breakpoints from "../../shared/breakpoints";

export const Container = styled.nav`
  z-index: 1100;
  position: fixed;
  left: 0;
  top: 0;
  width: 100vw;
  height: 5rem;
  

  ${breakpoints.desktop} {
    height: 7rem;
  }
`;



export const LinkList = styled.ul`
  position: absolute;
  display: none;
  height: 7rem;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  left: 0;
  background-color: ${colors.purple};
  width: 100%;
  text-align: left;
  justify-content: left;
  margin-left: 0;
  
  .nameTitle {
    font-size: 2rem;
    padding: 22px 0 0 20px;
    text-align: left;
    justify-content: left;
    font-weight: bolder;
    background-color: ${colors.purple};
    color: ${colors.pastel_blue} !important;
    text-shadow: 0px 2px 3px ${colors.purple};
    }
  }

  ${breakpoints.desktop} {
    //-webkit-backface-visibility: hidden;   /* Backface visibility stops the bug making LinkList flicker  */
    //backface-visibility: hidden;
    display: flex;
  }
`;

export const Bar = styled.span`
  position: absolute;
  background-color: ${colors.light_purple};
  top: 6.8rem;
  right: 0;
  height: 0.2rem;
  ${transitions.satanSnap};
  opacity: ${({ active }) => active};
  width: ${({ position }) => position.width}px;

  /* Pass in active to reset position if mouse has left list area.*/
  transform: translateX(
    ${({ position, active }) => (active > 0 ? position.left : 0)}px
  );
`;

export const MenuButton = styled.button`
  position: absolute;
  cursor: pointer;
  display: flex;
  right: calc(100vw / 12);
  height: 5rem;
  padding-top: 30px;

  /* Backface visibility stops the bug making LinkList flicker */
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;

  svg {
    fill: ${colors.white};

  }

  ${breakpoints.desktop} {
    display: none;
  }
`;
