import React from "react";
import { Link } from "gatsby";
import styled from "styled-components";
import { animated } from "react-spring";
import colors from "../../shared/colors";
import breakpoints from "../../shared/breakpoints";
import navArray from "../../shared/navigationArray";

const NavDrawer = ({ toggleNav, animation }) => {
  return (
    <Drawer
      style={{ transform: animation.negative.interpolate(y => `translate3d(0, -${y}vh, 0)`) }}>
      <DrawerList>
        {/*<DrawerLink onClick={toggleNav}>*/}
        {/*  <Link activeClassName="active" to="/">*/}
        {/*    Home*/}
        {/*  </Link>*/}
        {/*</DrawerLink>*/}
        {navArray.map((item, key) => (
          <DrawerLink key={key} onClick={toggleNav}>
            <Link activeClassName="active" to={item.to}>
              {item.text}
            </Link>
          </DrawerLink>
        ))}
      </DrawerList>
    </Drawer>
  );
};

const Drawer = styled(animated.div)`
  position: fixed;
  z-index: 1000;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  background-color: ${colors.purple};

  ${breakpoints.desktop} {
    display: none;
  }
`;

const DrawerList = styled.ul`
  position: relative;
  width: calc(100vw - ((100vw / 12) * 2));
  text-align: center;
  padding: 7rem calc(100vw / 12) 0rem;
  
  a {
    color: ${colors.platinum};    
    font-size: 50px;
    
    &:hover {
        color: ${colors.light_purple};
    }
  }
  
  li {
    margin: 0 0 20px 0;
  }
`;

const DrawerLink = styled.li`
    padding-bottom: 3.5rem;

   .active {
    color: ${colors.white};
   }
`;


export default NavDrawer;
