import React from "react";
import NavLink from "./NavLink";
import navArray from "../../shared/navigationArray";
import { Container, LinkList, MenuButton } from "./NavStyles";
import { FaBars, FaTimes } from "react-icons/fa";
import styled from "styled-components";
import colors from "../../shared/colors";
import downloadFile from '../../material/Pernille_Matthews_CV.pdf';
import ArrowDownwardSharpIcon from '@mui/icons-material/ArrowDownwardSharp';

const Nav = ({ toggleNav, visible }) => {
  return (
    <Container>
      <LinkList>
          <p className="nameTitle">Pernille Matthews | </p>
        {navArray.map((item, key) => (
          <NavLink
            key={key}
            text={item.text}
            to={item.to}
          />
        ))}
          <Wrapper>
              <a href={downloadFile} download>CV<ArrowDownwardSharpIcon/></a>{` `}
          </Wrapper>
      </LinkList>
      <MenuButton onClick={toggleNav} sx={{ paddingTop: '30px' }}>
        {!visible ? <FaBars size={30} /> : <FaTimes size={40} />}
      </MenuButton>
    </Container>
  );
};

export default Nav;

const Wrapper = styled.li`
  position: relative;
  margin-left: 5rem;
  display: flex;
  align-items: center;
  height: 7rem;
  justify-content: end;
  padding-right: 40px;

   a {
      color: ${colors.platinum};
      underline: none;
      text-decoration: none !important;
     
     &:hover {
        color: ${colors.light_purple};
     }
   }
`;
