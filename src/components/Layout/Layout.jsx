import React, { useState } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";
import colors from "../../shared/colors";
import "./styles/init.css";
import "./styles/type.css";
import Nav from "../Navigation/Nav";
import NavDrawer from "../Navigation/NavDrawer";
import { useSpring, animated, config } from "react-spring";
import CssBaseline from '@mui/material/CssBaseline';
import {amber, deepOrange, grey} from "@mui/material/colors";
import {Box, createTheme} from "@mui/material";
import {ThemeProvider} from "@mui/material/styles";
// import {useSiteMetadata} from ".Q./../hooks/use-site-metadata";
// import {useProjectsList} from "../../hooks/use-projects-list";
import "katex/dist/katex.min.css"
// require(katex/dist/katex.min-css)

const Layout = ({ children }) => {
    const [mode, setMode] = React.useState('dark');
    const colorMode = React.useMemo(
        () => ({
            toggleColorMode: () => {
                setMode((prevMode) => (prevMode === 'dark' ? 'light' : 'dark'));
            },
        }),
        [],
    );

    const [visible, setVisible] = useState(false);
    const toggleNav = () => setVisible(!visible);
    const ColorModeContext = React.createContext({ toggleColorMode: () => {} });
    // const { site } = useSiteMetadata();

    const animation = useSpring({
        positive: visible ? 100 : 0,
        negative: visible ? 0 : 100,
        config: config.slow,
    });

    const theme = createTheme({
        palette: {
            mode,
            ...(mode === 'light') ? {
                // palette values for light mode
                primary: amber,
                divider: amber[200],
                text: {
                    primary: colors.background_black,
                    secondary: grey[800],
                },
                background: {
                    default: amber[200],
                },

            } : {
                // palette values for dark mode
                primary: deepOrange,
                divider: deepOrange[700],
                background: {
                    default: colors.background_black,
                },
                text: {
                    primary: colors.platinum,
                    secondary: colors.platinum,
                },

            }
        },
        typography: {
            fontFamily: 'Consolas',
        },
        components: {
            MuiCssBaseline: {
                styleOverrides: {
                    body: {
                        // height: '100vh',
                        // overflow: 'hidden',
                        fontFamily: 'Consolas',
                        a: {
                            color: colors.light_purple,
                            '&:hover': {
                                color: colors.platinum,
                            }
                        },

                    },
                }
            },

        }
    });

    return (
        <>

            <ColorModeContext.Provider value={colorMode}>
                <ThemeProvider theme={theme}>
                <CssBaseline />

                <Nav toggleNav={toggleNav} visible={visible} animation={animation}  className="navi"/>
                    <NavDrawer
                        toggleNav={toggleNav}
                        visible={visible}
                        animation={animation}
                    />
                    <Box
                    sx={{
                        display: 'flex',
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'end',
                        bgcolor: 'background.default',
                        color: 'text.primary',
                        borderRadius: 1,
                        p: 8,

                    }}
                >
                    {/*<IconButton sx={{ ml: 1, mt: 5}} onClick={colorMode.toggleColorMode} color="inherit">*/}
                    {/*    {theme.palette.mode === 'dark' ?  <LightModeIcon  /> : <DarkModeIcon />}*/}
                    {/*</IconButton>*/}
                </Box>
                <Main>{children}</Main>
                </ThemeProvider>
            </ColorModeContext.Provider>
        </>
    );
};


const Main = styled(animated.main)`
  .tl-edges {
    overflow-x: hidden;

    .tl-wrapper {
      position: relative;
      z-index: 0;
      display: grid;
      grid-template-columns: repeat(12, 1fr);
      grid-column-gap: 1rem;
      // color: ${colors.white};
      // background-color: ${colors.background_black};
      min-height: 100vh;
    }
  }
`;

Layout.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Layout;
