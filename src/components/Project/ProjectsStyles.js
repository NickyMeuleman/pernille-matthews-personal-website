import styled from "styled-components";
import colors from "../../shared/colors";

export const ProjectWrapper = styled.main`
  p {
    line-height: 2.6rem;
    margin-bottom: 10px;    
  }
  
  .gatsby-resp-image-image {
    height: 100% !important; 
    width: 100% !important; 
    
    //padding-bottom: 20px;
  }

  h1 {
    
  }
  h4{
    padding-top: 10px;
    font-size: 25px;
    
  }
  
  h1, h2, h3, h4, h5, h6 {
    color: $colors.platinum;
  }
`;

