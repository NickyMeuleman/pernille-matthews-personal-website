---
slug: "/projects/explainable-artificial-intelligence"
date: "2022-10-07"
title: "Explainable AI (XAI)"
---

## What is XAI?
Currently in 2022, understanding machine learning is still a relatively new topic. 
However, it is becoming more and more important to understand how machine learning works. 
This is because it is the basis of the future of AI. 

