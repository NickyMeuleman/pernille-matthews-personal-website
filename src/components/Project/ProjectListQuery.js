import { useStaticQuery, graphql } from "gatsby"
export const usePostQuery = () => {
    const { allMdx } = useStaticQuery(
        graphql`
            query PostQuery {
                allMdx(filter: { fileAbsolutePath: { regex: "/projects/" } }) {
                    edges {
                        node {
                            frontmatter {
                                title
                            }
                        }
                    }
                }
            }
        `
    )
    return allMdx
}