import { graphql } from 'gatsby'
import { MDXRenderer } from 'gatsby-plugin-mdx'
import React from 'react'
import {ContentContainer} from "../Common/Container";
import {ProjectWrapper} from "../Project/ProjectsStyles";
import Grid from '@mui/material/Grid';
import "katex/dist/katex.min.css"


export const query = graphql
        `
        query PostsByID($id: String!) {
            mdx(
                id: { eq: $id }
            ){
                body
                frontmatter {
                    title
                    subTitle
                    date
                    tags
                }
            }
        }
    `

const ProjectList = ({ data }) => {
    const { frontmatter, body } = data.mdx
    return (
        <>
            <ContentContainer>
                <ProjectWrapper className="right">
                    <Grid container spacing={0}>
                        <Grid item xs={12}>
                            <p>{frontmatter.date}</p>
                        </Grid>
                        <Grid item xs={12}>
                          <h2>{frontmatter.title} {frontmatter.subTitle}</h2>
                        </Grid>

                        <Grid item xs={12}>
                            <MDXRenderer>{body}</MDXRenderer>
                        </Grid>
                    </Grid>
                </ProjectWrapper>
            </ContentContainer>
        </>
    )
}

export default ProjectList;