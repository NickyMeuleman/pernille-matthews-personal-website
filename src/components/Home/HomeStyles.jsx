import styled from "styled-components";
import breakpoints from "../../shared/breakpoints";
// import Div100vh from "react-div-100vh";
import colors from "../../shared/colors";

// export const HomeContainer = styled(Div100vh)`
//   align-items: center;
//   justify-content: center;
//   margin-bottom: 0rem;
//   // color: ${colors.platinum};
//
//   ${breakpoints.tablet} {
//     grid-column: 3 / 11;
//   }
// `;

export const Text = styled.div`
  p {
    margin: 1rem 0 5rem 0;
    ${breakpoints.desktop} {
      margin: 1rem 0 8rem 0;
    }
  }
`;

export const Socials = styled.div`
    display: flex;
    flex-direction: row;
    justify-content: center;
    padding-top: 20px;
    font-size: 40em;    
    
    .LinkedIn {
      color: ${colors.blue};
    }

    .Email {
      color: ${colors.white};
    }

    .GitHub {
      color: ${colors.platinum};
    }

    .Twitter {
      color: ${colors.baby_blue};
    }

    .LinkedIn, .Email, .GitHub, .Twitter {
      font-size: 1.5em;
      margin: 0 30px 0 30px;
      &:hover {
        color: ${colors.light_purple};
      }
    }
`;

export const Image = styled.div`
  border-radius: 2px;
  text-align: center;

  .gatsby-image-wrapper {
    border-radius:2px;
  }
  
  
  ${breakpoints.desktop} {
    width: 100%;
    border-radius:2px;


  }
`;
