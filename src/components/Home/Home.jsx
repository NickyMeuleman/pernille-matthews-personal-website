import React from "react";
import {Image, Socials} from "./HomeStyles";
import {EnhanceText} from "../Common/TextLink";
import {GatsbyImage} from "gatsby-plugin-image";
import { useStaticQuery, graphql } from "gatsby";
// import {ContentContainer} from "../Common/Container";
import { FaGithub, FaLinkedinIn, FaEnvelopeOpen, FaTwitter } from "react-icons/fa";

const Home = () => {
    const { file } = useStaticQuery(graphql`
    query {
      file(relativePath: { eq: "pernille_billede.jpg" }) {
        childImageSharp {
            gatsbyImageData(quality: 100 layout: CONSTRAINED placeholder: BLURRED height: 300 )
        }
      }
    }
    `);

    return (
        <>
            <div className="full">
            </div>

            <div className="imgCol" >

                <Image>
                    <GatsbyImage image={file.childImageSharp.gatsbyImageData} alt="Photo of Pernille Matthews" />
                </Image>
                <p className="subTitles">
                    <EnhanceText>
                        Pernille Matthews
                    </EnhanceText>
                    <br />
                    <p>
                        Research Assistant @ <a href="https://www.sdu.dk/en" target="_blank" rel="noopener noreferrer">
                        SDU
                    </a>
                        <br />
                    </p>
                </p>
                <div className="long">
                    <h4 className="center">I'm always up for a chat.<br/>Feel free to contact me!👋 ️</h4>
                    <Socials>
                        <a href="https://www.linkedin.com/in/pernillematthews" target="_blank"  rel="noopener noreferrer">
                            <FaLinkedinIn className="LinkedIn"/>
                        </a>

                        <a href="mailto: pernille@intcom.dk" target="_blank"  rel="noopener noreferrer">
                            <FaEnvelopeOpen className="Email"/>
                        </a>

                        <a href="https://github.com/PernilleMatthews" target="_blank"  rel="noopener noreferrer">
                            <FaGithub className="GitHub"/>
                        </a>

                        <a href="https://twitter.com/PernilMatthews" target="_blank"  rel="noopener noreferrer">
                            <FaTwitter className="Twitter"/>
                        </a>
                    </Socials>
                </div>
            </div>
            <div className="right">
                <h1>Greetings! </h1>
                <p className="mainText">
                    I am working part-time as a Research Assistant in the Department of Mathematics and Computer Science at the <a href="https://www.sdu.dk/da/om_sdu/institutter_centre/imada_matematik_og_datalogi" target="_blank"
                       rel="noopener noreferrer"> University of
                        Southern Denmark (SDU)</a>.
                </p>
                <br/>
                <p className="mainText">
                    My current research focus lies within Explainable AI (XAI). I believe that XAI is a field that will enable further development of AI and be of utmost importance to high-risk domains, such as health care, the finance sector and more.
                </p>
                <br/>
                <p className="mainText">
                    Besides academic work, I will soon start part-time at <a href="https://www.regionsjaelland.dk/omregionen/Organisation/oevrigeafdelingerogenheder/Data-og-Udviklingsstoette/Sider/Om-Produktion,-Forskning-og-Innovation.aspx"
                                                                             target="_blank"
                                                                             rel="noopener noreferrer">
                    Region Sjælland </a>
                    in the Data and Development department, where I will research and apply Machine Learning methods and XAI to health data.


                </p>
                <br/>
                <p className="mainText">

                    Lastly, I have a company, <a href="https://www.primecoding.dk/" target="_blank"
                                                       rel="noopener noreferrer">Prime Coding ApS</a>, where I work on smaller consultancy and developer-type
                    jobs.



                </p>
                <br/>
                <p className="mainText">
                    The purpose of this site is to share some bits about myself, but most importantly, to share projects and topics that I am interested in, and maybe you are too?                </p>

            <div>

            </div>
            </div>

        </>
    );
};

export default Home;
