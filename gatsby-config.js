// const remarkMath = require('remark-math');
// const rehypeKatex = require('rehype-katex');
// require = require('esm')(module);
require.esm = require('./require-esm')

module.exports = {
  // flags: {
  //   DEV_SSR: true
  // },
  siteMetadata: {
    title: `Pernille Matthews`,
    description: `Portfolio Website displaying projects and interests.`,
    author: `Pernille Matthews`,
    siteUrl: `https://pernillematthews.com/`,

  },

  plugins: [
      {
        resolve: `gatsby-source-filesystem`,
        options: {
          path: `${__dirname}/static/img`,
          name: `img`,
        },
      },

      {
        resolve: `gatsby-source-filesystem`,
        options: {
          path: `${__dirname}/src/projects`,
          name: `projects`,
        },
      },
      {
        resolve: "gatsby-source-filesystem",
        options: {
          path: `${__dirname}/src/images`,
          name: "images",
        },
      },
      `gatsby-transformer-sharp`,
      `gatsby-plugin-sharp`,
      {
        resolve: `gatsby-plugin-manifest`,
        options: {
          name: `Pernille Matthews`,
          short_name: `PM`,
          start_url: `/`,
          background_color: `#222222`,
          theme_color: `#222222`,
          icon: `src/images/favicon.png`,
        },
      },
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [{
          resolve: "gatsby-remark-external-links",
          options: {
            target: "_blank",
            rel: "noopener noreferrer"
            },
          },
          // { resolve: `gatsby-remark-katex`,
          //   options: {
          //     strict: `ignore`
          //   },
          // },
        ]
      },
    },
    {
      resolve: "gatsby-transformer-remark",
      options: {
        plugins: [
          {
            resolve: "gatsby-remark-relative-images",
            options: {
              linkImagesToOriginal: false,
              withWebp: true,
              // name: "uploads",
            },
          },
          {
            resolve: "gatsby-remark-images",
            options: {
              maxWidth: 650,
              linkImagesToOriginal: false,
              showCaptions: true
            },
          },
          {
            resolve: "gatsby-remark-copy-linked-files",
            options: {
              destinationDir: "static",
            },
          },
        ],
      },
    },
    {
      resolve: `gatsby-plugin-transition-link`,
      options: {
        layout: require.resolve(`./src/components/Layout/Layout.jsx`),
      },
    },
    {
      resolve: `gatsby-plugin-mdx`,
      options: {
        extensions: [`.mdx`, `.md`],
        remarkPlugins: [require.esm('remark-math')],
        rehypePlugins: [require.esm('rehype-katex')],
        gatsbyRemarkPlugins: [
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 600,
            },
          },
        ],
      },
    },
  ],
};
